#!/bin/bash

# Install openssh-server if not already installed to enable SSH connections
if ! command -v openssh-server &> /dev/null; then
    echo "openssh-server is not installed. Installing..."
    sudo apt update
    sudo apt install -yq openssh-server
fi

# Install socat if not already installed to enable link between Windows and Linux
if ! command -v socat &> /dev/null; then
    echo "socat is not installed. Installing..."
    sudo apt update
    sudo apt install -yq socat
fi

# Install wget if not already installed to enable download of npiperelay.exe
if ! command -v wget &> /dev/null; then
    echo "wget is not installed. Installing..."
    sudo apt update
    sudo apt install -yq wget
fi

# Install 7z if not already installed to enable extraction of npiperelay.exe
if ! command -v 7z &> /dev/null; then
    echo "7z is not installed. Installing..."
    sudo apt update
    sudo apt install -yq p7zip-full
fi

# Get npiperelay.exe
rm /tmp/npiperelay_windows_amd64.zip
rm /tmp/npiperelay_windows_amd64
wget https://github.com/jstarks/npiperelay/releases/download/v0.1.0/npiperelay_windows_amd64.zip -P /tmp
sudo 7z e -y /tmp/npiperelay_windows_amd64.zip -o/tmp/npiperelay_windows_amd64
sudo mv /tmp/npiperelay_windows_amd64/npiperelay.exe /usr/local/bin/npiperelay.exe
sudo chmod +x /usr/local/bin/npiperelay.exe
rm /tmp/npiperelay_windows_amd64.zip
rm /tmp/npiperelay_windows_amd64

# Place npiperelay.exe under Windows file system to avoid freezing delays
mkdir -p /mnt/c/wslUtilities
if [[ ! -f /mnt/c/wslUtilities/npiperelay.exe ]]; then
  sudo mv /usr/local/bin/npiperelay.exe /mnt/c/wslUtilities/npiperelay.exe
else
  sudo rm /usr/local/bin/npiperelay.exe
fi
sudo ln -s /mnt/c/wslUtilities/npiperelay.exe /usr/local/bin/npiperelay.exe

# Unsure socket exists
mkdir -p "${HOME}"/.ssh
touch "${HOME}"/.ssh/agent.sock

# Get agent forwarder script
wget https://gitlab.com/-/snippets/2506177/raw/main/wsl-ssh-agent-forwarder.sh -P ~/bin
sudo chmod +x ~/bin/wsl-ssh-agent-forwarder.sh

# If not already present, append this to bashrc in order to load wsl-ssh-agent-forwarder automatically
if ! grep -q "wsl-ssh-agent-forwarder.sh" ~/.bashrc; then
  printf "#SSH agent forwarding\nsource ~/bin/wsl-ssh-agent-forwarder.sh" >> ~/.bashrc
fi

echo -e "\e[42m\n"
echo -e "  WSL SSH agent setup complete!"
echo -e "  You must now restart your terminal session to use the Windows SSH agent."
echo -e "\n\e[0m"